import numpy as np
import trimesh # for converting voxel grids to meshes (to import objects into simulators)
# import time # to know how long it takes for the code to run
import os # to walk through directories, to rename files
import sys

import binvox_rw # to manipulate binvox files
import tools_sdf_generator

# Parses a file of type BINVOX
# Returns a voxel grid, generated using the binvox_rw.py package
def parse_BINVOX_file_into_voxel_grid(filename):
    filereader = open(filename, 'rb')
    binvox_model = binvox_rw.read_as_3d_array(filereader)
    voxelgrid = binvox_model.data
    return voxelgrid

if __name__ == "__main__":
    
    print("Usage: ")
    print("python binvox2sdf.py <FILEPATH>")
    
    filename = sys.argv[1]
    
    # Load the voxelgrid from file
    voxelgrid = parse_BINVOX_file_into_voxel_grid(filename)

    # Generate a folder to store the images
    print("Generating a folder to save the mesh")
    #directory = "./binvox2gazebo_" + str(time.time())
    # Generate a folder with the same name as the input file, without its ".binvox" extension
    currentPathGlobal = os.path.dirname(os.path.abspath(__file__))
    directory = currentPathGlobal + "/" + filename[:-7]
    if not os.path.exists(directory):
        os.makedirs(directory)

    mesh = trimesh.voxel.matrix_to_marching_cubes(
        matrix=voxelgrid,
        pitch=1.0,
        origin=(0,0,0))

    print("Merging vertices closer than a pre-set constant...")
    mesh.merge_vertices()
    print("Removing duplicate faces...")
    mesh.remove_duplicate_faces()
    print("Scaling...")
    mesh.apply_scale(scaling=1.0)
    print("Making the mesh watertight...")
    trimesh.repair.fill_holes(mesh)
    print("Fixing inversion and winding...")
    trimesh.repair.fix_inversion(mesh)
    trimesh.repair.fix_winding(mesh)

    
    
    print("Computing the center of mass: ")
    center_of_mass = mesh.center_mass
    print(center_of_mass)
    
    print("Computing moments of inertia: ")
    moments_of_inertia = mesh.moment_inertia
    print(moments_of_inertia)  # inertia tensor in meshlab

    print("Generating the STL mesh file")
    trimesh.exchange.export.export_mesh(
        mesh=mesh,
        file_obj=directory + "/mesh.stl",
        file_type="stl"
    )

    print("Generating the SDF file...")
    object_model_name = "mesh"
    # mass=1.00

    mass = 0.00
    for i in range(16, 48, 1):
        for j in range(28, 36, 1):
            for k in range(16, 48, 1):
                if (voxelgrid[k][i][j] == True):
                    mass += 1.00
    print("Calculated mass (equal to number of filled voxels: {0}".format(mass))

    tools_sdf_generator.generate_model_sdf(
        directory=directory,
        object_name=object_model_name,
        center_of_mass=center_of_mass,
        inertia_tensor=moments_of_inertia,
        mass=mass,
        model_stl_path=directory + "/mesh.stl")
